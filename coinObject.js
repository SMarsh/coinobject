const main_div = document.getElementById('main')
const coin = {
    state: 0,
    flip: function() {
        this.state = Math.floor(Math.random()*2)
       
        console.log(coin.state)
        return this.state 
        //should generate either 1 or 0
    },

    // 1. One point: Randomly set your coin object's "state" property to be either 

    //    0 or 1: use "this.state" to access the "state" property on this object.

    //2. One point: Return the string "Heads" or "Tails", depending on whether
        //    "this.state" is 0 or 1.
    toString: function() {
        if (this.state === 1) {
            console.log("this is heads")
            return 'Heads';
        } else {
            console.log("this is tails")
            return 'Tails';
        }
    },
 

    toHTML: function() {
        const image = document.createElement('img');
        if (this.state === 1) {
            image.src = 'img/QtrHeads.png';
        } else {
            image.src = 'img/Qtrtails.png';
        }
        main_div.appendChild(image)
        return image;
    }
};
 


//SHOULD display array consisting of 20 flips (1 or 0)
function display20Flips() {
    const results = [];
    for (let i = 1; i <= 20; i++) {
      coin.flip() 
      
        results.push(coin.toString());
    }
    
    return results;
}


//Displays 20 Images (Heads or Tails)
function display20Images() {
    const results = [];
    for (i = 0; i < 20; i++) {
        let coinFlip = coin.flip();
        results.push(coinFlip);
        coin.toHTML();
    }
    ('Display 20 Images:', results)
    return results;
}
display20Images();
console.log(display20Images())

//Note that you will need to download or create images to use to display face-up or face-down coins for the toHTML method. Create a folder directory named images in your repository to hold them.

//Also note that, by convention, an object's toString() method should always return a string which represents the object. This return value can then be used in any debugging output you may need to do. This will be a helpful convention to follow going forward. However, a toString() method should not directly produce any visual output itself – that is, for example, it shouldn't itself perform a console.log(): it should only return the string.

//Test and demonstrate that your coin object is complete doing the following:

//One point: Write a function called display20Flips that uses a loop to flip the coin 20 times. Each time the coin flips, display the result of each flip as a string on the page (make use of your toString() method) and also push the result of the flip to the array 'results'. After your loop completes, return the results array.

//One point: Write a function called display20Images, again using a loop to flip the coin 20 times…but this time instead of displaying the result as a string, display the result of each flip as an HTML IMG element on the page (make use of your toHTML() method). Also push each result to the array 'results' and return the results array after your loop completes.

//Your HTML file should have nothing in the body except for your script tag. Instead of hard-coding HTML elements, create them dynamically with Javascript and append them to your HTML


